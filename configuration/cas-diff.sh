#!/bin/sh
for i in $(find src/main/webapp -type f|grep -v .svn)
do
	if [[ -f ../../workspace/cas-server-3.5.2/cas-server-webapp/$i ]]
	then
		echo "diff $i"
		diff -urN ../../workspace/cas-server-3.5.2/cas-server-webapp/$i $i >cas-diff-$(basename $i).patch
	else
		echo "does not exist $i"
	fi
done
for i in $(find src/main/java -type f|grep -v .svn)
do
	if ls -1 ../../workspace/cas-server-3.5.2/*/${i}
	then
		echo "diff $i"
		diff -urN ../../workspace/cas-server-3.5.2/*/$i $i >cas-diff-$(basename $i).patch
	else
		echo "does not exist $i"
	fi
done
