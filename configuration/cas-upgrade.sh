#!/bin/sh
for i in $(find src/main/webapp -type f|grep -v .svn)
do
	if [[ -f ../../workspace/cas-server-3.4.12/cas-server-webapp/$i ]]
	then
		echo "diff $i"
		diff -urN ../../workspace/cas-server-3.4.12/cas-server-webapp/$i ../../workspace/cas-server-3.5.2/cas-server-webapp/$i >cas-upgrade-$(basename $i).patch
		patch -p5 <cas-upgrade-$(basename $i).patch
	else
		echo "does not exist $i"
	fi
done
for i in $(find src/main/java -type f|grep -v .svn)
do
	if ls -1 ../../workspace/cas-server-3.4.12/*/${i}
	then
		echo "diff $i"
		diff -urN ../../workspace/cas-server-3.4.12/*/$i ../../workspace/cas-server-3.5.2/*/$i >cas-upgrade-$(basename $i).patch
		patch -p5 <cas-upgrade-$(basename $i).patch
	else
		echo "does not exist $i"
	fi
done
