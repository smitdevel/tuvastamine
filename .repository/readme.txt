This project local repo is used to avoid using the "lib" folder and system scope dependencies.

Local library install example: 
C:\CAS\Projects\saauth>mvn install:install-file -DgroupId=ee.sk.jDigiDoc -DartifactId=digidoc-client -Dversion=1.0 -Dpackaging=jar -Dfile=lib/JDigiDoc.jar -DlocalRepositoryPath=.repository

-- Nortal CAS (add sources/classes with -Dclassifier=sources/classes)
C:\DEV\Siseministeerium\DHS\cas>mvn install:install-file -Dfile=../nortal-cas/pom.xml -DgroupId=com.nortal -DartifactId=nortal-cas -Dversion=1.0.1-SNAPSHOT -Dpackaging=pom -DlocalRepositoryPath=.repository

C:\DEV\Siseministeerium\DHS\cas>mvn install:install-file -Dfile=../nortal-cas/nortal-cas-webapp/target/nortal-cas-webapp.war -DpomFile=../nortal-cas/nortal-cas-webapp/pom.xml -DlocalRepositoryPath=.repository

C:\DEV\Siseministeerium\DHS\cas>mvn install:install-file -Dfile=../nortal-cas/nortal-cas-webapp/target/nortal-cas-webapp-classes.jar -DpomFile=../nortal-cas/nortal-cas-webapp/pom.xml -DlocalRepositoryPath=.repository -Dclassifier=classes

C:\DEV\Siseministeerium\DHS\cas>mvn install:install-file -Dfile=../nortal-cas/nortal-cas-webapp/target/nortal-cas-webapp-sources.jar -DpomFile=../nortal-cas/nortal-cas-webapp/pom.xml -DlocalRepositoryPath=.repository -Dclassifier=sources

C:\DEV\Siseministeerium\DHS\cas>mvn install:install-file -Dfile=../nortal-cas/nortal-cas-webapp/target/nortal-cas-webapp-classes.jar -DgroupId=com.nortal -DartifactId=nortal-cas-webapp -Dversion=1.0.1-SNAPSHOT -Dpackaging=jar -DlocalRepositoryPath=.repository

C:\DEV\Siseministeerium\DHS\cas>mvn install:install-file -Dfile=../nortal-cas/nortal-cas-support-esteid/target/nortal-cas-support-esteid-1.0.1-SNAPSHOT.jar -DpomFile=../nortal-cas/nortal-cas-support-esteid/pom.xml -DlocalRepositoryPath=.repository

C:\DEV\Siseministeerium\DHS\cas>mvn install:install-file -Dfile=../nortal-cas/nortal-cas-support-mobileid/target/nortal-cas-support-mobileid-1.0.1-SNAPSHOT.jar -DpomFile=../nortal-cas/nortal-cas-support-mobileid/pom.xml -DlocalRepositoryPath=.repository

C:\DEV\Siseministeerium\DHS\cas>mvn install:install-file -Dfile=../nortal-cas/nortal-cas-support-x509/target/nortal-cas-support-x509-1.0.1-SNAPSHOT.jar -DpomFile=../nortal-cas/nortal-cas-support-x509/pom.xml -DlocalRepositoryPath=.repository