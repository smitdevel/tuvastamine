/**
 * @author Allar Saarnak Get id-card certificates and add them to session^M
 * @author Kaarel Jõgeva iframe based solution for NGINX compatibility^M
 */
$('.get-esteid-certificate').on('click', function (event) {
    event.preventDefault();
    if (useAjaxLoader()) {
    	ajaxLoader();
    } else {
        iFrameLoader($(this).parent());
    }
});

function useAjaxLoader() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/rv:11/i)) {
        return true;
    }

    return false;
}

function ajaxLoader() {
    jQuery.ajax({
        url: idCardUrl, type: 'GET', tryCount: 0, retryLimit: 3,
        cache : false,
        xhrFields: {
            withCredentials: true
         },
        crossDomain: true,
        success: function () {
            $('#idCardLoginForm').submit();
        },
        error: function (xhr) {
            if (xhr.status == 400) { // In IE10 SSL re-handshake error occurs, second try is successful
                this.tryCount++;
                if (this.tryCount <= this.retryLimit) {
                    $.ajax(this);
                    return;
                }
                return;
            }
        }
    });
}

function iFrameLoader(iFrameParent) {
    $('<iframe />', {
        name: 'idCardCredentials',
        id: 'idCardCredentials',
        src: idCardUrl,
        style: 'display: none;'
    }).load(function () {
        $('#idCardLoginForm').submit();
    }).appendTo(iFrameParent);
}
