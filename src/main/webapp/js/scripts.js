$(document).ready(function() {
   "use strict";
   // tabs
   // Resolve currently active trigger (if any)
   var activeTrigger = document.URL.substring(document.URL.indexOf("#"), document.URL.length);

   $('.tabs').each(function() {
      var parent = $(this),
         triggers = $(this).find('.tablinks a'),
         tabs = parent.find('section');
      
      triggers.each(function() {
         var trigger = $(this), targetId = trigger.attr('href'), target = $(targetId);
         if(trigger.hasClass('active') && targetId.length > 0) target.addClass('active');
         
         trigger.on('click', function(e) {
            e.preventDefault();
            
            if(target.length > 0) {
               triggers.removeClass('active');
               tabs.removeClass('active');
               trigger.addClass('active');
               target.addClass('active');
               updateWindowLocation(targetId);
            }
         })
         
         // Check if we need to activate a trigger
         if (targetId === activeTrigger && !target.is(':visible')) {
            trigger.click();
         }
      })
   })
   
   function updateWindowLocation(target) {
      // Support only in-page references
      if (target.lastIndexOf("#", 0) !== 0) {
         throw "Target must start with a '#'"
      }

      var baseUrl = document.URL;
      var hashIndex = baseUrl.indexOf("#");
      if (hashIndex >= -1) {
         baseUrl = baseUrl.substring(0, hashIndex);
      }

      window.location = (baseUrl + target);
   }

});
