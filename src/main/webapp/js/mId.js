/**
 * @author Priit Liivak
 * @author Allar Saarnak
 */
MobileIdStatus = {
        cancelled: false
};
$(function() {
    MobileIdStatus.cancelled=false;
    $('#mobileIdButton').click(function() {
       var phonenumber = $('#phonenumber').val();
       if (phonenumber) {
          phonenumber = phonenumber.replace(/\s/g, "");
       }

       if (!phonenumber || !$.isNumeric(phonenumber)) {
          $('#mIdNumberRequired').show();
          hideCancel();
          return false;
       }
       $('#phonenumber').val(phonenumber); // Send "validated" number

       var idCode = $('#idCode').val();
       if (idCode) {
          idCode = jQuery.trim(idCode);
       }

       if (!idCode) {
          showMessage('Isikukood on kohustuslik väli.');
          hideCancel();
          return false;
       }

       $('#mIdNumberRequired').hide();
       $("#mIdMessageTextError").hide();
       $('#mobileIdCancel').show();
       startMobileIdAuth($('#mobileIdButton').attr('messageAfterClick'));
       
       return false; 
    });
});

function hideCancel(){
   $('#mobileId_actions').show();
   $('#mobileIdCancel').hide();
}

function hideMessage() {
    $('#mobileId_message').hide();
    $('#mIdMessageText').hide()
    $('#mobileId_actions').show();
}
function showMessage(content) {
    if (content!==undefined) {
        var dialogContainer = $('#mobileId_message');
        dialogContainer.find('#mIdMessageText').html(content).show();
        //Make sure that message is visible
        dialogContainer.show();
        $('#mobileId_actions').hide();
    }
}
function cancelMobileIdAuth() {
    MobileIdStatus.cancelled=true;
    hideMessage();
    $('#mobileIdCancel').hide();
}
function startMobileIdAuth(initialMsg) {
    MobileIdStatus.cancelled = false;
     
    showMessage(initialMsg);
    $.ajax({
        type : "POST",
        url : "mobileIdAuth",
        data : {
            action : "m_id_login",
            phonenumber : $("#phonenumber").val(),
            idCode : $("#idCode").val(),
            countryCode : $("#countryCode").val()
        },
        success : mobileIdStartAuthenticationResponse,
        error : mobileIdError,
        dataType : "json",
        cache : false,
        global : false
    });
    return;
}
function mobileIdStartAuthenticationResponse(data, textStatus, XMLHttpRequest) {
    if(MobileIdStatus.cancelled){
        MobileIdStatus.cancelled=false;
        return;
    }
    showMessage(data.message);
    var attemptDelay = data.attemptDelay;
     
    if (data.startStatusCheck) {
        setTimeout(function() {
            $.ajax({
                type : "POST",
                url : "mobileIdAuth",
                data : {
                    action : "m_id_status",
                    phonenumber : $("#phonenumber").val(),
                    idCode : $("#idCode").val(),
                    countryCode : $("#countryCode").val(),
                    sessionCode : data.sessionCode
                },
                success : mobileIdStartAuthenticationResponse,
                error : mobileIdError,
                dataType : "json",
                cache : false,
                global : false
            });
        }, attemptDelay * 1000);
    } else if (data.doCheckCertificate) {
       $('#mobileIdLoginForm').submit();
    }
}
function mobileIdError(event, request, settings) {
    $('#mobileIdCancel').hide();
    $('#mIdMessageTextError').show();
    $('#mobileId_message').show();
    $('#mobileId_actions').show();
}