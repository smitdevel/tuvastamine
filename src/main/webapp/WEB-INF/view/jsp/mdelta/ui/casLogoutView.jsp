<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:directive.include file="includes/top.jsp" />

<h1><spring:message code="screen.logout.header" /></h1>

<div class="notifications">
	<p class="confirmation"><spring:message code="screen.logout.success" /></p>
</div>

<c:set var="returnUrl" value="${header['referer']}" />
<c:if test="${not empty returnUrl}">
   <p><a href='<c:url value="${returnUrl}" />'><spring:message code="screen.logout.return" /></a></p>
</c:if>

<p><spring:message code="screen.logout.security" /></p>
<jsp:directive.include file="includes/bottom.jsp" />