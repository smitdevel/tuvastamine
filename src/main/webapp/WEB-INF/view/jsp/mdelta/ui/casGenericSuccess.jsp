<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:directive.include file="includes/top.jsp" />

<h1><spring:message code="screen.success.header" /></h1>

<div class="notifications">
   <p class="confirmation"><spring:message code="screen.success.success" /></p>
</div>

<p><spring:message code="screen.success.security" /></p>
<jsp:directive.include file="includes/bottom.jsp" />