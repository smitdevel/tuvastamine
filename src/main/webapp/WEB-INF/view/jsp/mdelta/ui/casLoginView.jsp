<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<jsp:directive.include file="includes/top.jsp" />

<c:if test="${not pageContext.request.secure}">
   <div id="msg" class="errors">
      <h2><spring:message code="login.non_secure" text="Non-secure Connection" /></h2>
      <p><spring:message code="login.non_secure.description" text="You are currently accessing CAS over a non-secure connection. Single Sign On WILL NOT WORK. In order to have single sign on work, you MUST log in over HTTPS" />.</p>
   </div>
</c:if>

<h1>Delta</h1>
<p><spring:message code="screen.welcome" text="Please choose a preferred method to log in" />:</p>
<div class="tabs">
   <ul class="tablinks">
      <li><a href="#login" class="active"><spring:message code="login.username" text="Username" /></a></li>
      <li><a href="#login-idcard"><spring:message code="login.id_card" text="ID-card" /></a></li>
      <li><a href="#login-mobile"><spring:message code="login.mobile_id" text="Mobile-ID" /></a></li>
   </ul>

   <section id="login" class="active">
      <form:form method="post" id="fm1" commandName="${commandName}" htmlEscape="true">
         <div class="notifications">
            <form:errors path="*" id="msg" cssClass="error" element="p" />
         </div>
         
         <c:if test="${not empty sessionScope.openIdLocalId}">
            <strong>${sessionScope.openIdLocalId}</strong>
            <input type="hidden" id="username" name="username" value="${sessionScope.openIdLocalId}" />
         </c:if>
         
         <ol class="form small">
            <li class="formrow">
               <label for="username" class="formlabel"><spring:message code="login.username" />:</label> 
               <span class="formcontent"> 
                  <form:input cssClass="required" cssErrorClass="error" id="username" tabindex="1" path="username" autocomplete="${loginAutocompleteAllowed}" htmlEscape="true" />
               </span>
            </li>
            <li class="formrow">
               <label for="password" class="formlabel"><spring:message code="login.password" />:</label> 
               <span class="formcontent">
                  <form:password cssClass="required" cssErrorClass="error" id="password" tabindex="2" path="password" htmlEscape="true" autocomplete="${loginAutocompleteAllowed}" />
               </span>
            </li>
            <li class="formrow">
               <div class="buttongroup">
                  <input type="hidden" name="lt" value="${loginTicket}" />
                  <input type="hidden" name="execution" value="${flowExecutionKey}" />
                  <input type="hidden" name="_eventId" value="submit" />
                  <button name="submit" accesskey="l" tabindex="4" type="submit"><spring:message code="screen.welcome.button.login" /></button>
               </div>
            </li>
         </ol>
      </form:form>
   </section>
   <section id="login-idcard">
      <form:form method="post" id="idCardLoginForm" commandName="${commandName}" htmlEscape="true">
         <p class="form single">
            <img src='<c:url value="/images/id-logo-s.png"/>' alt="ID kaart" /><br />
            <spring:message code="login.id_card.insert_card" />
            <input type="hidden" name="lt" value="${loginTicket}" />
            <input type="hidden" name="execution" value="${flowExecutionKey}" />
            <input type="hidden" name="_eventId" value="submitIdCard" />
            <c:choose>
               <c:when test="${not empty param.TARGET}">
                  <!-- "TARGET" is for SAML11 and "service" is for CAS20 protocol -->
                  <input type="hidden" name="TARGET" value="${param.TARGET}" />
               </c:when>
               <c:otherwise>
                  <input type="hidden" name="service" value="${param.service}" />
               </c:otherwise>
            </c:choose>
            <input type="hidden" name="renew" value="${param.renew}" />
            <input type="hidden" name="gateway" value="${param.gateway}" />

            <button class="get-esteid-certificate"><spring:message code="screen.welcome.button.login" /></button>
         </p>
      </form:form>
   </section>
   <section id="login-mobile">
      <p class="form single">
         <img src='<c:url value="/images/id-logo-s.png"/>' alt="Mobiil-ID" />
      </p>
      
      <form:form method="post" id="mobileIdLoginForm" commandName="${commandName}" htmlEscape="true">
         <input type="hidden" name="lt" value="${loginTicket}" />
         <input type="hidden" name="execution" value="${flowExecutionKey}" />
         <input type="hidden" name="_eventId" value="submitMobileId" />
         <c:choose>
            <c:when test="${not empty param.TARGET}">
               <!-- "TARGET" is for SAML11 and "service" is for CAS20 protocol -->
               <input type="hidden" name="TARGET" value="${param.TARGET}" />
            </c:when>
            <c:otherwise>
               <input type="hidden" name="service" value="${param.service}" />
            </c:otherwise>
         </c:choose>
         <input type="hidden" name="renew" value="${param.renew}" />
         <input type="hidden" name="gateway" value="${param.gateway}" />
      
         <!-- Various messages -->
         <div id="mobileId_message">
             <div class="notifications">
               <p id="mIdMessageText" class="warning" style="display: none;"></p>
               <p id="mIdMessageTextError" class="error" style="display: none;"><spring:message code="login.mobile_id.error.failed" text="Error, failed" /></p>
               <p id="mIdNumberRequired" class="error" style="display: none;"><spring:message code="login.mobile_id.rm.error.canContainOnlyDigits" text="Only digits allowed" /></p>
             </div>
         </div>
         <!-- End various messages -->

         <ol class="form small">
            <li class="formrow"><label for="phonenumber" class="formlabel"><spring:message code="login.mobile_id.label.phoneNumber" text="Phone number" />:</label> <span class="formcontent"><input type="text" id="phonenumber" name="phonenumber"></span></li>
            <li class="formrow"><label for="idCode" class="formlabel"><spring:message code="login.mobile_id.label.idCode" text="Identity code" />:</label> <span class="formcontent"><input type="text" id="idCode" name="idCode"></span></li>
            <li class="formrow">
               <div class="buttongroup">
                  <button
                        id="mobileIdButton"
                        messageAfterClick="<spring:message code="login.mobile_id.startingSession" text="Starting session" />"
                        type="submit" class="button button-confirm" tabindex="3"><spring:message code="screen.welcome.button.login" /></button>
                  <button id="mobileIdCancel" type="button" class="button button-confirm" onclick="cancelMobileIdAuth()" style="display:none;">
                    <spring:message code="login.mobile_id.label.cancelbutton" text="Cancel" />
                  </button>
               </div>
            </li>
         </ol>
      </form:form>
   </section>
</div>

<jsp:directive.include file="includes/bottom.jsp" />