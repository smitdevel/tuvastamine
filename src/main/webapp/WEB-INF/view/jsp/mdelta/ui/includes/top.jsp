<!DOCTYPE html>
<%@ page session="true" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html class="no-js">
<head>
	<meta charset="utf-16" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title><spring:message code="login" text="Login" /></title>
	<meta name="author" content="Nortal" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="shortcut icon" href='<c:url value="/images/favicon.ico" />' />
	<link rel="stylesheet" href='<c:url value="/css/main.css" />' />
	<script>
		document.documentElement.className = document.documentElement.className.replace('no-js','js');
		if('ontouchstart' in window || 'onmsgesturechange' in window) document.documentElement.className += ' touch';
	</script>
   
    <body>
	<header>
		<div class="headerwrapper">
			<a id="logo" href="#"></a>
		</div>
	</header>
	<div class="contentwrapper">