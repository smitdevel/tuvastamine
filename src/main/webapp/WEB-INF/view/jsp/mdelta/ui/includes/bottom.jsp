      </div>
      <footer>
         <br />
      </footer>
      
      <!--Jquery 1.9.1 is required by idCard.js-->
      <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
      <script src='<c:url value="/js/scripts.js" />'></script>
      <!-- Set ID card certificate check URL -->
      <script type="text/javascript">
      <%
      org.springframework.web.context.WebApplicationContext context = org.springframework.web.context.support.WebApplicationContextUtils.getWebApplicationContext(application);
      com.nortal.auth.esteid.EsteidConfigurationInitializer esteidConfigurationInitializer = (com.nortal.auth.esteid.EsteidConfigurationInitializer) context.getBean("esteidConfigurationInitializer");
      pageContext.setAttribute("idCardBaseUrl", (esteidConfigurationInitializer.getIdCardAuthUrl() + request.getContextPath()));
	  %>
      	var idCardUrl = '<c:url value="${idCardBaseUrl}/idlogin" />';
      </script>
      <script type="text/javascript" src='<c:url value="/js/mId.js" />'></script>
      <script type="text/javascript" src='<c:url value="/js/idCard.js" />'></script>
   </body>  
</html>
