package org.jasig.cas.support.spnego.web.flow;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jasig.cas.web.support.WebUtils;
import org.springframework.util.StringUtils;
import org.springframework.webflow.action.AbstractAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

public final class SpnegoNegociateCredentialsAction extends AbstractAction {
    private boolean ntlm = false;
    private List<String> supportedBrowser;
    private String messageBeginPrefix = this.constructMessagePrefix();

    public SpnegoNegociateCredentialsAction() {
    }

    protected Event doExecute(RequestContext context) {
        HttpServletRequest request = WebUtils.getHttpServletRequest(context);
        HttpServletResponse response = WebUtils.getHttpServletResponse(context);
        String authorizationHeader = request.getHeader("Authorization");
        String userAgent = request.getHeader("User-Agent");
        if(StringUtils.hasText(userAgent) && this.isSupportedBrowser(userAgent) && (!StringUtils.hasText(authorizationHeader) || !authorizationHeader.startsWith(this.messageBeginPrefix) || authorizationHeader.length() <= this.messageBeginPrefix.length())) {
            if(this.logger.isDebugEnabled()) {
                this.logger.debug("Authorization header not found. Sending WWW-Authenticate header");
            }

            response.setHeader("WWW-Authenticate", this.ntlm?"NTLM":"Negotiate");
            response.setStatus(401);
        }

        return this.success();
    }

    public void setNtlm(boolean ntlm) {
        this.ntlm = ntlm;
        this.messageBeginPrefix = this.constructMessagePrefix();
    }

    public void setSupportedBrowser(List<String> supportedBrowser) {
        this.supportedBrowser = supportedBrowser;
    }

    public void afterPropertiesSet() throws Exception {
        if(this.supportedBrowser == null) {
            this.supportedBrowser = new ArrayList();
            this.supportedBrowser.add("MSIE");
            this.supportedBrowser.add("Trident");
            this.supportedBrowser.add("Firefox");
            this.supportedBrowser.add("AppleWebKit");
        }

    }

    protected String constructMessagePrefix() {
        return (this.ntlm?"NTLM":"Negotiate") + " ";
    }

    protected boolean isSupportedBrowser(String userAgent) {
        Iterator i$ = this.supportedBrowser.iterator();

        String supportedBrowser;
        do {
            if(!i$.hasNext()) {
                return false;
            }

            supportedBrowser = (String)i$.next();
        } while(!userAgent.contains(supportedBrowser));

        return true;
    }
}
