package com.nortal.cas.support.login;

import org.springframework.webflow.action.AbstractAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import com.nortal.auth.util.PropertiesUtil;

public class SetupAllowRememberPasswordAction extends AbstractAction {

    @Override
    protected Event doExecute(final RequestContext context) throws Exception {
        boolean autocompleteAllowed = Boolean.parseBoolean(PropertiesUtil.getProperty("login.autocomplete.allowed"));
        context.getFlowScope().put("loginAutocompleteAllowed", autocompleteAllowed ? "on" : "off");
        return result("success");
    }

}
