package com.nortal.cas.support.x509;

import static org.apache.commons.lang.StringUtils.substringAfter;
import static org.apache.commons.lang.StringUtils.substringBefore;
import static org.apache.commons.lang.StringUtils.trimToNull;

import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jasig.cas.authentication.principal.Credentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nortal.cas.adaptors.x509.X509Credentials;
import com.nortal.cas.adaptors.x509.X509CredentialsToPrincipalResolver;
import com.nortal.cas.support.esteid.id.EsteidCredentials;

public class IdCodeX509CredentialsToPrincipalResolver extends X509CredentialsToPrincipalResolver {
	
	private final String COMMON_NAME = "CN";
	
	private static final Logger log = LoggerFactory.getLogger(IdCodeX509CredentialsToPrincipalResolver.class);
	
	@Override
	protected String resolvePrincipalInternal(final X509Certificate certificate) {
		String certificateSubject = certificate.getSubjectDN().getName();
		if (log.isInfoEnabled()) {
			log.info("Creating principal for: " + certificateSubject);
		}
		String[] subjectItems = certificateSubject.split(", ");
		Map<String, String> nameFields = new HashMap<String, String>();
		
		for(String item : subjectItems){
			String[] keyValue = item.split("=");
			nameFields.put(keyValue[0], keyValue[1]);
		}
		
		String commonName = nameFields.get(COMMON_NAME).replace("\"", "");
		String[] parsedCN = commonName.split(",");
		
		String idCode = parsedCN[2];
		String firstName = parsedCN[1];
		String lastName = parsedCN[0];
		String countryCode = nameFields.get(COUNTRY_CODE);
		
		return resolvePrincipal(idCode, countryCode, firstName, lastName);
	}

	@Override
	protected String resolvePrincipal(String idCode, String countryCode, String firstName, String lastName) {
		return idCode;
	}

	@Override
	public boolean supports(Credentials credentials) {
		return super.supports(credentials);
	}

}
