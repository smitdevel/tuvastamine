package com.nortal.cas.support.mobileid;

import static com.nortal.cas.support.mobileid.MobileidMessage.BAD_MOBILE_ID_AUTH;
import static org.slf4j.LoggerFactory.getLogger;

import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import org.apache.commons.lang.StringUtils;
import org.apache.tools.ant.filters.StringInputStream;
import org.jasig.cas.authentication.handler.AuthenticationException;
import org.jasig.cas.authentication.handler.BadCredentialsAuthenticationException;
import org.slf4j.Logger;

import com.nortal.cas.adaptors.x509.X509Credentials;
import com.nortal.cas.support.mobileid.enums.MobileIdStatus;
import com.nortal.cas.support.mobileid.model.MobileIdAuth;

public class MobileIdCredentials extends X509Credentials {
	private static final Logger logger = getLogger(MobileIdCredentials.class);

	private static final long serialVersionUID = 1L;

	private MobileIdAuth mobileIdAuthResult;

	private static final String START_CERTIFICATE = "-----BEGIN CERTIFICATE-----\n";

	private static final String END_CERTIFICATE = "\n-----END CERTIFICATE-----\n";

	public MobileIdCredentials(MobileIdAuth mobileIdAuthResult) {
		this.mobileIdAuthResult = mobileIdAuthResult;
	}

	@Override
	public String getBadCredentialsMessageCode() {
		return BAD_MOBILE_ID_AUTH.getMessageCode();
	}

	@Override
	public X509Certificate[] getNotValidatedCertificatesAuthenticationHandler() throws AuthenticationException {

		if (mobileIdAuthResult == null || StringUtils.isBlank(mobileIdAuthResult.getCertificateData())
				|| !MobileIdStatus.USER_AUTHENTICATED.equals(mobileIdAuthResult.getStatus())) {
			if (logger.isDebugEnabled()) {
				logger.debug("Certificates not found in mobile auth result or status is incorrect: " + mobileIdAuthResult);
			}
			throw new BadCredentialsAuthenticationException(getBadCredentialsMessageCode());
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Certificate found in mobile id auth result.");
		}

		X509Certificate cert = null;
		StringBuilder fullCertificate = new StringBuilder(START_CERTIFICATE);
		try {
			CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
			fullCertificate.append(mobileIdAuthResult.getCertificateData());
			fullCertificate.append(END_CERTIFICATE);

			if (logger.isTraceEnabled()) {
				logger.trace("parsing certificate for: " + mobileIdAuthResult.getPhoneNo() + " cert:\n"
						+ fullCertificate.toString());
			}

			cert = (X509Certificate) certificateFactory
					.generateCertificate(new StringInputStream(fullCertificate.toString()));
		}
		catch (CertificateException e) {
			logger.error("Could not parse mobile id certificate for: " + mobileIdAuthResult.getPhoneNo() + " Certificate:\n"
					+ fullCertificate.toString(), e);
			e.printStackTrace();
			throw new BadCredentialsAuthenticationException(getBadCredentialsMessageCode());
		}
		return new X509Certificate[] { cert };
	}

	public String getAuthenticationPhoneNumber() {
		if (mobileIdAuthResult != null && mobileIdAuthResult.getPhoneNo() != null) {
			return new String(mobileIdAuthResult.getPhoneNo());
		}

		return null;
	}
}
