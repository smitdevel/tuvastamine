package com.nortal.cas.support.mobileid.service.impl;

import org.jasig.cas.authentication.Authentication;
import org.jasig.cas.authentication.AuthenticationMetaDataPopulator;
import org.jasig.cas.authentication.principal.Credentials;

import com.nortal.cas.support.mobileid.MobileIdCredentials;

public class MobileIdAuthenticationMetaDataPopulator implements AuthenticationMetaDataPopulator {

	@Override
	public Authentication populateAttributes(Authentication authentication, Credentials credentials) {
		if (credentials instanceof MobileIdCredentials) {
			MobileIdCredentials mIdCredentials = (MobileIdCredentials) credentials;
			authentication.getAttributes().put("phoneNumber", mIdCredentials.getAuthenticationPhoneNumber());
		}
		return authentication;
	}

}
