package com.nortal.cas.support.esteid.id;

import java.security.cert.X509Certificate;

import javax.validation.constraints.NotNull;

import org.jasig.cas.authentication.handler.AuthenticationException;
import org.jasig.cas.authentication.handler.BadCredentialsAuthenticationException;
import org.jasig.cas.authentication.principal.Credentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nortal.auth.esteid.EsteidConfigurationInitializer;
import com.nortal.cas.adaptors.x509.X509Credentials;
import com.nortal.cas.adaptors.x509.X509CredentialsAuthenticationHandler;

import ee.sk.digidoc.DigiDocException;
import ee.sk.digidoc.factory.NotaryFactory;

public class EsteidAuthenticationHandler extends X509CredentialsAuthenticationHandler {

  private final Logger log = LoggerFactory.getLogger(getClass());
  
	@NotNull
	private EsteidConfigurationInitializer esteidConfigurationInitializer;

	@Override
	public boolean supports(Credentials credentials) {
		return credentials != null && EsteidCredentials.class.isAssignableFrom(credentials.getClass());
	}

	@Override
	protected void validateCredentials(boolean validCaCertificate, boolean hasTrustedIssuer, X509Certificate clientCert,
			X509Credentials credentials) throws AuthenticationException {
		super.validateCredentials(validCaCertificate, hasTrustedIssuer, clientCert, credentials);
		validateClientCert(clientCert, credentials);
	}
	/**
	 * Extra validation logic for {@link X509CredentialsAuthenticationHandler}.
	 * @throws AuthenticationException
	 */
	protected void validateClientCert(X509Certificate clientCert, X509Credentials credentials)
			throws AuthenticationException {
		if (!validateCertificate(clientCert)) {
			log.debug("Certificate was found to be invalid");
			throw new BadCredentialsAuthenticationException(credentials.getBadCredentialsMessageCode());
		}
	}

	/**
	 * Special validation logic that applies to Estonian id card validation.
	 * Checks if id card was closed or stolen!
	 * @param cert Certificate that has allready passed generic validations.
	 * @return true if valid or validation disabled
	 */
	private boolean validateCertificate(X509Certificate cert) {
		if (!esteidConfigurationInitializer.isOcspEnabled()) {
			log.info("OCSP is disabled, skipping ID-card validation.");
			return true;
		}

		if (log.isDebugEnabled()) {
			log.debug("--examining cert["
					+ cert.getSerialNumber().toString() + "] "
					+ cert.getSubjectDN() + "\"" + " from issuer \""
					+ cert.getIssuerDN().getName() + "\"");
		}

		NotaryFactory notary = esteidConfigurationInitializer.getNotary();
		try {
			notary.checkCertificate(cert);
			log.info("Id card validation OK");
		}
		catch (DigiDocException e) {
			log.error("Certificate is not valid", e);
			return false;
		}
		catch (Throwable e) {
			log.error("Certificate validation failed with unkown exception", e);
			throw new RuntimeException(e);
		}
		return true;
	}

	public void setEsteidConfigurationInitializer(EsteidConfigurationInitializer esteidConfigurationInitializer) {
		this.esteidConfigurationInitializer = esteidConfigurationInitializer;
	}
}
