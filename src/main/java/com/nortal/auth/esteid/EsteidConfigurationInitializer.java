/**
 * 
 */
package com.nortal.auth.esteid;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.util.Hashtable;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.StringUtils;

import ee.sk.digidoc.DigiDocException;
import ee.sk.digidoc.factory.NotaryFactory;
import ee.sk.utils.ConfigManager;

/**
 * @author Priit Liivak
 * @author Alar Kvell
 * 
 */
public class EsteidConfigurationInitializer implements InitializingBean {
  private static final Logger log = LoggerFactory.getLogger(EsteidConfigurationInitializer.class);

  private NotaryFactory notary;
  
  private boolean ocspEnabled = true;
  private boolean test = false;

  private String idCardAuthUrl;
  private String pkcs12Container;
  private String pkcs12Password;
  private String pkcs12CertSerial;

  public NotaryFactory getNotary() {
    return notary;
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    configureJDigiDoc();
  }

  private void configureJDigiDoc() {
    if (!ocspEnabled) {
      log.info("Not configuring JDigiDoc, OCSP is not enabled!");
      return;
    }

    log.info("Configuring JDigiDoc");
    Security.addProvider(new BouncyCastleProvider());

		if (StringUtils.hasLength(pkcs12Container)) {
			File container = new File(pkcs12Container);
			if (!container.canRead()) {
				throw new RuntimeException("Cannot read PKCS12 container file: " + container);
			}
			if (!container.isFile()) {
				throw new RuntimeException("PKCS12 container is not a regular file: " + container);
			}
			if (!StringUtils.hasText(pkcs12Password)) {
				throw new RuntimeException("PKCS12 container password must not be empty");
			}
			if (!StringUtils.hasText(pkcs12CertSerial)) {
				throw new RuntimeException("PKCS12 certificate serial number must not be empty");
			}
			Hashtable<String, String> props = new Hashtable<String, String>();
			props.put("SIGN_OCSP_REQUESTS", "true");
			props.put("DIGIDOC_PKCS12_CONTAINER", pkcs12Container);
			props.put("DIGIDOC_PKCS12_PASSWD", pkcs12Password);
			props.put("DIGIDOC_OCSP_SIGN_CERT_SERIAL", pkcs12CertSerial);
			log.info("Signing OCSP requests with certificate (serial number " + pkcs12CertSerial + ") from PKCS12 container " + pkcs12Container);
			// This must be done first, because init(Hashtable) resets configuration
			ConfigManager.init(props);
		}

		// This is done second, because init(String) does not reset configuration
		String config = "jdigidoc" + (isTest() ? "-test" : "") + ".cfg";
		log.info("Initializing configuration with " + config);
    if (ConfigManager.init("jar://" + config)) {
      log.info("JDigiDoc is initialized, configuring Notary...");
      try {
        this.notary = ConfigManager.instance().getNotaryFactory();
        log.info("Notary is initialized!");
      } catch (DigiDocException e) {
        log.error("Notary is not initialized! ID card athentication doesn't work!", e);
        throw new RuntimeException("Notary is not initialized! ID card athentication doesn't work!", e);
      }
    } else {
      log.error("JDigiDoc is not initialized! ID card athentication doesn't work!");
      // jdigidoc hides the exceptions, only logs some error info and returns false from init.
      // So throw our own exception on init failure.
      throw new RuntimeException("JDigidoc is not initialized! ID card athentication doesn't work!");
    }

    try {
      MessageDigest.getInstance("SHA1", BouncyCastleProvider.PROVIDER_NAME);
    } catch (NoSuchAlgorithmException e1) {
      log.error("Provider initialization error", e1);
      throw new RuntimeException("Provider initialization error", e1);
    } catch (NoSuchProviderException e1) {
      log.error("Provider initialization error", e1);
      throw new RuntimeException("Provider initialization error", e1);
    }
  }

	public void setOcspEnabled(boolean ocspEnabled) {
		this.ocspEnabled = ocspEnabled;
	}

	public boolean isOcspEnabled() {
		return ocspEnabled;
	}
	
    public void setTest(boolean test) {
        this.test = test;
    }

    public boolean isTest() {
        return test;
    }	

	public String getIdCardAuthUrl() {
		return idCardAuthUrl;
	}

	public void setIdCardAuthUrl(String idCardAuthUrl) {
		this.idCardAuthUrl = idCardAuthUrl;
	}

	public void setPkcs12Container(String pkcs12Container) {
		this.pkcs12Container = pkcs12Container;
	}

	public void setPkcs12Password(String pkcs12Password) {
		this.pkcs12Password = pkcs12Password;
	}

	public void setPkcs12CertSerial(String pkcs12CertSerial) {
		this.pkcs12CertSerial = pkcs12CertSerial;
	}
}
